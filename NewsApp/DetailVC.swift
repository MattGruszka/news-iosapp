//
//  DetailVC.swift
//  NewsApp
//
//  Created by Gruszkovsky on 10.06.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//

import Foundation
import UIKit


class DetailVC: ViewController {
    
    
    
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var myLabel: UILabel?
    @IBOutlet weak var timestamp: UILabel?
    @IBOutlet weak var wholeText: UILabel?
    @IBOutlet weak var webView: UIWebView?
    
    var myTitleTmp: String = ""
    var timestampTmp: String = ""
    var wholeTextTmp: String = ""
    var backImageTmp: UIImage? = nil
    var tag: String = "#"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myLabel?.text = myTitleTmp
        timestamp?.text = timestampTmp
        wholeText?.text = wholeTextTmp
        backImage?.image = backImageTmp
        self.title = tag
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
