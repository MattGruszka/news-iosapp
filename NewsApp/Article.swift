//
//  Article.swift
//  NewsApp
//
//  Created by Gruszkovsky on 11.06.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//

import Foundation

class Article {
    var title = String()
    var imgURL = String()
    var timestamp = Int()
    var wholeText = String()
    var tag = String()
    
    init(pTitle: String, pImgURL: String?, pTimestamp: Int, pWholeText: String, pTag: String) {
        title = pTitle
        if (pImgURL == nil) {
            imgURL = "noImage"
        } else {
            imgURL = pImgURL!
        }
        timestamp = pTimestamp
        wholeText = pWholeText
        tag = pTag
    }
    
    init(coder aDecoder: NSCoder!) {
        self.title = aDecoder.decodeObject(forKey: "title") as! String
        self.imgURL = aDecoder.decodeObject(forKey: "imgURL") as! String
        self.timestamp = aDecoder.decodeObject(forKey: "timestamp") as! Int
        self.wholeText = aDecoder.decodeObject(forKey: "wholeText") as! String
        self.tag = aDecoder.decodeObject(forKey: "tag") as! String

    }
    
    func initWithCoder(aDecoder: NSCoder) -> Article {
        self.title = aDecoder.decodeObject(forKey: "title") as! String
        self.imgURL = aDecoder.decodeObject(forKey: "imgURL") as! String
        self.timestamp = aDecoder.decodeObject(forKey: "timestamp") as! Int
        self.wholeText = aDecoder.decodeObject(forKey: "wholeText") as! String
        self.tag = aDecoder.decodeObject(forKey: "tag") as! String
        return self
    }
    
    func encodeWithCoder(aCoder: NSCoder!) {
        aCoder.encode(title, forKey: "title")
        aCoder.encode(imgURL, forKey: "imgURL")
        aCoder.encode(timestamp, forKey: "timestamp")
        aCoder.encode(wholeText, forKey: "wholeText")
        aCoder.encode(tag, forKey: "tag")

    }
    


    
}
