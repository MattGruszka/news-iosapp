//  This file was automatically generated and should not be edited.

import Apollo

public final class ViewControllerQuery: GraphQLQuery {
  public static let operationDefinition =
    "query ViewController {" +
    "  articles(t: [Article]) {" +
    "    __typename" +
    "    id" +
    "    title" +
    "    redacted_title" +
    "    ts" +
    "    body(t: [Plain]) {" +
    "      __typename" +
    "      data" +
    "    }" +
    "    img {" +
    "      __typename" +
    "      url" +
    "      b64" +
    "    }" +
    "    tags" +
    "    author {" +
    "      __typename" +
    "      img" +
    "      name" +
    "    }" +
    "  }" +
    "}"
  public init() {
  }

  public struct Data: GraphQLMappable {
    /// Set of articles representing category in WP24 app (and wp.pl).
    public let articles: [Article?]?

    public init(reader: GraphQLResultReader) throws {
      articles = try reader.optionalList(for: Field(responseName: "articles", arguments: ["t": ["Article"]]))
    }

    public struct Article: GraphQLMappable {
      public let __typename: String
      /// Unique identifier of an article. Not guaranteed for direct url queries.
      public let id: String?
      /// Meta title of an article.
      public let title: String
      /// Redacted title - may differ from actual article title.
      public let redactedTitle: String?
      /// Article modification timestamp.
      public let ts: Int
      /// Consecutive paragraphs of the article.
      public let body: [Body?]?
      public let img: Img?
      public let tags: [String?]?
      public let author: Author?

      public init(reader: GraphQLResultReader) throws {
        __typename = try reader.value(for: Field(responseName: "__typename"))
        id = try reader.optionalValue(for: Field(responseName: "id"))
        title = try reader.value(for: Field(responseName: "title"))
        redactedTitle = try reader.optionalValue(for: Field(responseName: "redacted_title"))
        ts = try reader.value(for: Field(responseName: "ts"))
        body = try reader.optionalList(for: Field(responseName: "body", arguments: ["t": ["Plain"]]))
        img = try reader.optionalValue(for: Field(responseName: "img"))
        tags = try reader.optionalList(for: Field(responseName: "tags"))
        author = try reader.optionalValue(for: Field(responseName: "author"))
      }

      public struct Body: GraphQLMappable {
        public let __typename: String
        public let data: String

        public init(reader: GraphQLResultReader) throws {
          __typename = try reader.value(for: Field(responseName: "__typename"))
          data = try reader.value(for: Field(responseName: "data"))
        }
      }

      public struct Img: GraphQLMappable {
        public let __typename: String
        /// Entity url (if available directly from web).
        public let url: String
        /// Base64, header stripped, 42x42 representation of the image.
        public let b64: String?

        public init(reader: GraphQLResultReader) throws {
          __typename = try reader.value(for: Field(responseName: "__typename"))
          url = try reader.value(for: Field(responseName: "url"))
          b64 = try reader.optionalValue(for: Field(responseName: "b64"))
        }
      }

      public struct Author: GraphQLMappable {
        public let __typename: String
        public let img: String?
        public let name: String?

        public init(reader: GraphQLResultReader) throws {
          __typename = try reader.value(for: Field(responseName: "__typename"))
          img = try reader.optionalValue(for: Field(responseName: "img"))
          name = try reader.optionalValue(for: Field(responseName: "name"))
        }
      }
    }
  }
}