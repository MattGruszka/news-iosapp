//
//  ViewControllerTableViewCell.swift
//  NewsApp
//
//  Created by Gruszkovsky on 08.06.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//

import UIKit

class ViewControllerTableViewCell: UITableViewCell {

    @IBOutlet weak var timestamp: UILabel!
    @IBOutlet weak var myTitle: UILabel!
    @IBOutlet weak var myImage: UIImageView!
    
    let queue = SerialOperationQueue()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.myImage?.image = nil
        self.myTitle?.text = nil
    }

}
