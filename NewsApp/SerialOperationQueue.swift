//
//  SerialOperationQueue.swift
//  NewsApp
//
//  Created by Gruszkovsky on 09.06.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//

import Foundation

class SerialOperationQueue: OperationQueue {
    override init() {
        super.init()
        maxConcurrentOperationCount = 1
    }
}
