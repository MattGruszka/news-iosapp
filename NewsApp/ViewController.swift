//
//  ViewController.swift
//  NewsApp
//
//  Created by Gruszkovsky on 08.06.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//

import UIKit
import Apollo
import QuartzCore
import Foundation
import SystemConfiguration


class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView?
    
    let userDefaults = UserDefaults.standard
    var segment = 0
    let apollo = ApolloClient(url: URL(string: "https://mobileapi.wp.pl/v1/graphql")!)
    var articles: [Article] = [] {
        didSet{
            tableView?.reloadData()
        }
    }
    var articlesBefore: [ViewControllerQuery.Data.Article] = [] 
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (isInternetAvailable() == true ) {
            print("internet is avaible: \(isInternetAvailable())")
            loadData(segment: segment)
            let when = DispatchTime.now() + 2
            DispatchQueue.main.asyncAfter(deadline: when) {
            let tmpArticles = self.articlesConverter(articles: self.articlesBefore)
            print(tmpArticles)
            self.save(articlesToEncode: tmpArticles)
            
            self.articles = self.load()
            print(self.articles)
            }
            
        } else {
            articles = load()
            tableView?.reloadData()
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
            }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ViewControllerTableViewCell
        configureCell(cell: cell, atIndexPath: indexPath)
        cell.layer.borderWidth = 2.0
        cell.layer.borderColor = UIColor.red.cgColor
        return cell
    }
    
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextScene = storyboard?.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
        let i = indexPath.row
        nextScene.myTitleTmp = articles[i].title
        nextScene.wholeTextTmp = (articles[i].wholeText)
        
        if (articles[i].imgURL == nil) {
            nextScene.backImageTmp = #imageLiteral(resourceName: "tukan")
        } else {
            nextScene.backImageTmp = loadImage(url: (articles[i].imgURL))
        }
        nextScene.timestampTmp = calculatePostTime(time: articles[i].timestamp)
        nextScene.tag = nextScene.tag + (articles[i].tag)
        self.navigationController?.pushViewController(nextScene, animated: true)
     }
    
    func loadData(segment: Int) {
        apollo.fetch(query: ViewControllerQuery() ) { [weak self] result, error in
        guard let articlesBeforeTry = result?.data?.articles else { return }
        self?.articlesBefore = articlesBeforeTry as! [ViewControllerQuery.Data.Article]
        }
    }
    
    func loadImage(url: String) -> UIImage {
        let myImageURL = URL(string: url)
        let myImageData = NSData(contentsOf: myImageURL!)
        let image = UIImage(data: myImageData! as Data)
        return image!
    }
    
    func configureCell(cell: ViewControllerTableViewCell, atIndexPath indexPath: IndexPath){
        
        cell.queue.cancelAllOperations()
        let operation: BlockOperation = BlockOperation()
        operation.addExecutionBlock { Void in
            var image: UIImage
            
            if (self.articles[indexPath.row].imgURL == nil) {
                image = #imageLiteral(resourceName: "tukan")
            } else {
                image = self.loadImage(url: (self.articles[indexPath.row].imgURL))
            }
            let text = self.articles[indexPath.row].title
            DispatchQueue.main.sync(execute: { Void in
                cell.myImage.image = image
                cell.myTitle.text = text
                cell.timestamp.text = self.calculatePostTime(time: self.articles[indexPath.row].timestamp)
            })
        }
        cell.queue.addOperation(operation)
    }
    
    func calculatePostTime(time: Int) -> String {
        var retStr = ""
        let nowTimestamp = NSDate().timeIntervalSince1970
        let result =  (Int(nowTimestamp) - (time))/3600
        
        if (result < 1) {
             retStr = "Less then an hour ago."
        } else if (result == 1) {
             retStr = "An hour ago."
        }
        else if (result < 24 && result > 1) {
             retStr = "\(Int(result)) hours ago."
        }
        else if (result < 48) {
             retStr = "One day ago."
        }
        else if (result > 48) {
            let tmp: Int = Int(result) / 24
            retStr = "\(tmp) days ago."
        }
        return retStr

    }
    
    func isInternetAvailable() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    
    func articlesConverter(articles: [ViewControllerQuery.Data.Article] ) -> [Article] {
        var articlesToEncode: [Article] = []
        for oldArticle in articles {
            print("-------")
            print("title \(oldArticle.title)")
            //print((oldArticle.img?.url)!)
            print(oldArticle.ts)
            print((oldArticle.body?[0]?.data)!)
            //print((oldArticle.tags?[0]!)! )
            print("-------")
            
            var imgTry = oldArticle.img
            var str = "noImage"
            if (imgTry != nil) {
                 str = (oldArticle.img?.url)!
            }
            var tmpTag = ""
            var boolTest = oldArticle.tags?.isEmpty
            if (boolTest)! {
               tmpTag = "Polska"
            } else {
               tmpTag = (oldArticle.tags?[0])!
            }
            
            let newArticle = Article(pTitle: oldArticle.title, pImgURL: str, pTimestamp: oldArticle.ts, pWholeText: (oldArticle.body?[0]?.data)!, pTag: tmpTag)
            
            articlesToEncode.append(newArticle)
        }
        return articlesToEncode
    }
    
    func save(articlesToEncode: [Article]) {
        print("IN SAVE")
        print("to ENCODE: \(articlesToEncode)")
        var arrayWithArraysToEncode: [[Data]] = []
        for oneArticle in articlesToEncode {
            let encodedTitle = NSKeyedArchiver.archivedData(withRootObject: oneArticle.title)
            let encodedImg = NSKeyedArchiver.archivedData(withRootObject: oneArticle.imgURL)
            let encodedText = NSKeyedArchiver.archivedData(withRootObject: oneArticle.wholeText)
            let encodedTimestamp = NSKeyedArchiver.archivedData(withRootObject: oneArticle.timestamp)
            let encodedTags = NSKeyedArchiver.archivedData(withRootObject: oneArticle.tag)
            
            var encodedArray: [Data] = [encodedTitle, encodedImg, encodedText, encodedTimestamp, encodedTags]
            arrayWithArraysToEncode.append(encodedArray)
        }
        print("saved")
        userDefaults.set(arrayWithArraysToEncode, forKey: "allArticles")
        userDefaults.synchronize()
    }
    
    func load() -> [Article] {
        print("LOAD SECTION")
        var dataAllArticles: [[Data]] = userDefaults.object(forKey: "allArticles") as! [[Data]]
        var unpackedArticles: [Article] = []
        print(dataAllArticles)
        print("0")
        for oneArticle in dataAllArticles{
            print("0")
            var unpackedTitle: String = NSKeyedUnarchiver.unarchiveObject(with: oneArticle[0] as Data) as! String
            var unpackedImg: String = NSKeyedUnarchiver.unarchiveObject(with: oneArticle[1] as Data) as! String
            var unpackedText: String = NSKeyedUnarchiver.unarchiveObject(with: oneArticle[2] as Data) as! String
            var unpackedTimestamp: Int = NSKeyedUnarchiver.unarchiveObject(with: oneArticle[3] as Data) as! Int
            var unpackedTags: String = NSKeyedUnarchiver.unarchiveObject(with: oneArticle[4] as Data) as! String
            
            var article = Article(pTitle: unpackedTitle, pImgURL: unpackedImg, pTimestamp: unpackedTimestamp, pWholeText: unpackedText, pTag: unpackedTags)
            unpackedArticles.append(article)
        }
        print("tu \(unpackedArticles)")
        return unpackedArticles
    }
    
}
